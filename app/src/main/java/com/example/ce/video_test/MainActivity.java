package com.example.ce.video_test;


import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;

public class MainActivity extends AppCompatActivity implements SurfaceHolder.Callback{
    private Camera mCamera;
    private SurfaceHolder surfaceHolder;
    private SurfaceView surfaceView;
    boolean status;
    private Context myContext;
     boolean recording = false;
    Bundle bundle;
    private MediaRecorder mediaRecorder;
    private String  fileuri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bundle = savedInstanceState;
        setContentView(R.layout.activity_main);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        myContext = this;
        status = false;
        surfaceView = (SurfaceView) findViewById(R.id.surfaceView);
        surfaceHolder = surfaceView.getHolder();
        surfaceHolder.addCallback(this);
        surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        final Button start = (Button) findViewById(R.id.button);
        chooseCamera();
        final Handler handler = new Handler(){
            @Override
            public void handleMessage(Message msg) {
                switch(msg.what)
                {
                    case 1 : start.setText("stop");
                        break;
                    case 2 : start.setText("start");
                        break;
                    default:break;
                }
            }
        };

       start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (recording) {
                    mediaRecorder.stop(); // stop the recording
                    releaseMediaRecorder(); // release the MediaRecorder object
                    Toast.makeText(MainActivity.this, "Video captured!", Toast.LENGTH_LONG).show();
                    start.setText("start");
                    if(recording) {
                        launchUploadActivity();
                        recording = false;
                    }
                } else {
                   if (!prepareMediaRecorder()) {
                        Toast.makeText(MainActivity.this, "Fail in prepareMediaRecorder()!\n - Ended -", Toast.LENGTH_LONG).show();
                        finish();
                      }
                   new Thread(new Runnable() {
                         public void run() {
                           try {
                                mediaRecorder.start();
                                handler.sendEmptyMessage(1);
                             } catch (final Exception ex) {
                             }
                             recording = true;
                         }
                     }).start();
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Thread.sleep(60*1000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            mediaRecorder.stop();
                            releaseMediaRecorder();
                            handler.sendEmptyMessage(2);

                            if(recording) {
                                launchUploadActivity();
                                recording = false;
                            }
                        }
                    }).start();


                }
            }
        });
    }


    private boolean prepareMediaRecorder() {
         mediaRecorder = new MediaRecorder();
         mCamera.unlock();
         mediaRecorder.setCamera(mCamera);
         mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
         mediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
         mediaRecorder.setProfile(CamcorderProfile.get(CamcorderProfile.QUALITY_HIGH));
        mediaRecorder.setPreviewDisplay(surfaceHolder.getSurface());
         mediaRecorder.setOutputFile( Environment.getExternalStoragePublicDirectory(
                 Environment.DIRECTORY_DCIM) + "/"
                 + System.currentTimeMillis() + ".mp4");
        fileuri = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DCIM) + "/"
                + System.currentTimeMillis() + ".mp4";
         try {
            mediaRecorder.prepare();
            } catch (IllegalStateException e) {
           releaseMediaRecorder();
            return false;
          } catch (IOException e) {
            releaseMediaRecorder();
            return false;
          }
         return true;
    }

    private boolean hasCamera(Context context) {

        if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            return true;
         } else {
            return false;
         }
    }

    private void launchUploadActivity(){
        Intent i = new Intent(MainActivity.this, UploadActivity.class);
        i.putExtra("filePath", fileuri);
    }
    public void chooseCamera() {
        int cameraId = findBackFacingCamera();
       if (cameraId >= 0)
       {
           mCamera = Camera.open(cameraId);
           refreshCamera(mCamera);
       }

    }

    public void refreshCamera(Camera camera) {
        if (surfaceHolder.getSurface() == null) {
            return;
        }
        try {
            camera.stopPreview();
        } catch (Exception e) {
        }

        try {
            camera.setPreviewDisplay(surfaceHolder);
            camera.startPreview();
        } catch (Exception e) {

        }
    }


    private int findBackFacingCamera() {
       int cameraId = -1;
       int numberOfCameras = Camera.getNumberOfCameras();
         for (int i = 0; i < numberOfCameras; i++) {
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(i, info);
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
                cameraId = i;
                 break;
              }
        }
        return cameraId;
       }
    @Override
    protected void onPause()
    {
        super.onPause();
        if(recording) {
            mediaRecorder.stop();
        }
        releaseCamera();
        status = true;
        finish();
    }
    public void onResume() {
        super.onResume();
        if (!hasCamera(myContext)) {
             Toast toast = Toast.makeText(myContext, "Sorry, your phone does not have a camera!", Toast.LENGTH_LONG);
            toast.show();
            finish();
        }
        else
        {
            if(status)
            {onCreate(bundle);}

        }
    }
    private void releaseMediaRecorder() {
        if (mediaRecorder != null) {
            mediaRecorder.reset(); // clear recorder configuration
            mediaRecorder.release(); // release the recorder object
            mediaRecorder = null;
            mCamera.lock(); // lock camera for later use
          }
      }

    private void releaseCamera() {
         if (mCamera != null) {
             mCamera.release();
             mCamera = null;
         }
     }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        try {
            if (mCamera == null) {
                mCamera.setPreviewDisplay(holder);
                mCamera.startPreview();
            }
        } catch (IOException e) {

        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        refreshCamera(mCamera);
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

    }
}
